using System;
using Xunit;
using BMI_Assignment_GT;

using static System.Console;


namespace UnitTest_BMI_Calculator
{
    public class UnitTest1
    {
        [Fact]
        public void TestInput1()
        {
            // arrange
            int a = 265;
            int b = 72;
            int expected = 36;
            var test = new Calculations.Calc1();
            // act
            double actual = test.BMI_Calc(a, b);
            // assert
            Assert.Equal(expected, actual);
           
        }

        [Fact]
        public void TestInput2()
        {
            // arrange
            int a = 200;
            int b = 72;
            int expected = 27;
            var test = new Calculations.Calc1();
            // act
            double actual = test.BMI_Calc(a, b);
            // assert
            Assert.Equal(expected, actual);

         

        }
    }
}
